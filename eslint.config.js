import { sxzz } from '@sxzz/eslint-config'

export default sxzz([], {
  vue: true,
  prettier: true,
  markdown: false,
  unocss: true,
  sortKeys: true,
})
