/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-router. ‼️ DO NOT MODIFY THIS FILE ‼️
// It's recommended to commit this file.
// Make sure to add this file to your tsconfig.json file as an "includes" or "files" entry.

/// <reference types="unplugin-vue-router/client" />

import type {
  // type safe route locations
  RouteLocationTypedList,
  RouteLocationResolvedTypedList,
  RouteLocationNormalizedTypedList,
  RouteLocationNormalizedLoadedTypedList,
  RouteLocationAsString,
  RouteLocationAsRelativeTypedList,
  RouteLocationAsPathTypedList,

  // helper types
  // route definitions
  RouteRecordInfo,
  ParamValue,
  ParamValueOneOrMore,
  ParamValueZeroOrMore,
  ParamValueZeroOrOne,

  // vue-router extensions
  _RouterTyped,
  RouterLinkTyped,
  RouterLinkPropsTyped,
  NavigationGuard,
  UseLinkFnTyped,

  // data fetching
  _DataLoader,
  _DefineLoaderOptions,
} from 'unplugin-vue-router/types'

declare module 'vue-router/auto/routes' {
  export interface RouteNamedMap {
    '/': RouteRecordInfo<'/', '/', Record<never, never>, Record<never, never>>,
    '/common-menu/environment-manager': RouteRecordInfo<'/common-menu/environment-manager', '/common-menu/environment-manager', Record<never, never>, Record<never, never>>,
    '/common-menu/project-manager': RouteRecordInfo<'/common-menu/project-manager', '/common-menu/project-manager', Record<never, never>, Record<never, never>>,
    '/common-menu/role-manager': RouteRecordInfo<'/common-menu/role-manager', '/common-menu/role-manager', Record<never, never>, Record<never, never>>,
    '/common-menu/user-manager': RouteRecordInfo<'/common-menu/user-manager', '/common-menu/user-manager', Record<never, never>, Record<never, never>>,
    '/home': RouteRecordInfo<'/home', '/home', Record<never, never>, Record<never, never>>,
    '/interface-automation/case/': RouteRecordInfo<'/interface-automation/case/', '/interface-automation/case', Record<never, never>, Record<never, never>>,
    '/interface-automation/case/new-or-edit': RouteRecordInfo<'/interface-automation/case/new-or-edit', '/interface-automation/case/new-or-edit', Record<never, never>, Record<never, never>>,
    '/interface-automation/manager/': RouteRecordInfo<'/interface-automation/manager/', '/interface-automation/manager', Record<never, never>, Record<never, never>>,
    '/interface-automation/manager/new-or-edit': RouteRecordInfo<'/interface-automation/manager/new-or-edit', '/interface-automation/manager/new-or-edit', Record<never, never>, Record<never, never>>,
    '/login': RouteRecordInfo<'/login', '/login', Record<never, never>, Record<never, never>>,
    '/register': RouteRecordInfo<'/register', '/register', Record<never, never>, Record<never, never>>,
    '/report/interface/': RouteRecordInfo<'/report/interface/', '/report/interface', Record<never, never>, Record<never, never>>,
    '/report/interface/details': RouteRecordInfo<'/report/interface/details', '/report/interface/details', Record<never, never>, Record<never, never>>,
    '/report/stress/': RouteRecordInfo<'/report/stress/', '/report/stress', Record<never, never>, Record<never, never>>,
    '/report/stress/details': RouteRecordInfo<'/report/stress/details', '/report/stress/details', Record<never, never>, Record<never, never>>,
    '/report/ui/': RouteRecordInfo<'/report/ui/', '/report/ui', Record<never, never>, Record<never, never>>,
    '/report/ui/details': RouteRecordInfo<'/report/ui/details', '/report/ui/details', Record<never, never>, Record<never, never>>,
    '/stress-test/manager/': RouteRecordInfo<'/stress-test/manager/', '/stress-test/manager', Record<never, never>, Record<never, never>>,
    '/stress-test/manager/new-or-edit': RouteRecordInfo<'/stress-test/manager/new-or-edit', '/stress-test/manager/new-or-edit', Record<never, never>, Record<never, never>>,
    '/test-plan/time-plan': RouteRecordInfo<'/test-plan/time-plan', '/test-plan/time-plan', Record<never, never>, Record<never, never>>,
    '/ui-automation/case/': RouteRecordInfo<'/ui-automation/case/', '/ui-automation/case', Record<never, never>, Record<never, never>>,
    '/ui-automation/case/new-or-edit': RouteRecordInfo<'/ui-automation/case/new-or-edit', '/ui-automation/case/new-or-edit', Record<never, never>, Record<never, never>>,
    '/ui-automation/elements/': RouteRecordInfo<'/ui-automation/elements/', '/ui-automation/elements', Record<never, never>, Record<never, never>>,
    '/ui-automation/elements/new-or-edit': RouteRecordInfo<'/ui-automation/elements/new-or-edit', '/ui-automation/elements/new-or-edit', Record<never, never>, Record<never, never>>,
  }
}

declare module 'vue-router/auto' {
  import type { RouteNamedMap } from 'vue-router/auto/routes'

  export type RouterTyped = _RouterTyped<RouteNamedMap>

  /**
   * Type safe version of `RouteLocationNormalized` (the type of `to` and `from` in navigation guards).
   * Allows passing the name of the route to be passed as a generic.
   */
  export type RouteLocationNormalized<Name extends keyof RouteNamedMap = keyof RouteNamedMap> = RouteLocationNormalizedTypedList<RouteNamedMap>[Name]

  /**
   * Type safe version of `RouteLocationNormalizedLoaded` (the return type of `useRoute()`).
   * Allows passing the name of the route to be passed as a generic.
   */
  export type RouteLocationNormalizedLoaded<Name extends keyof RouteNamedMap = keyof RouteNamedMap> = RouteLocationNormalizedLoadedTypedList<RouteNamedMap>[Name]

  /**
   * Type safe version of `RouteLocationResolved` (the returned route of `router.resolve()`).
   * Allows passing the name of the route to be passed as a generic.
   */
  export type RouteLocationResolved<Name extends keyof RouteNamedMap = keyof RouteNamedMap> = RouteLocationResolvedTypedList<RouteNamedMap>[Name]

  /**
   * Type safe version of `RouteLocation` . Allows passing the name of the route to be passed as a generic.
   */
  export type RouteLocation<Name extends keyof RouteNamedMap = keyof RouteNamedMap> = RouteLocationTypedList<RouteNamedMap>[Name]

  /**
   * Type safe version of `RouteLocationRaw` . Allows passing the name of the route to be passed as a generic.
   */
  export type RouteLocationRaw<Name extends keyof RouteNamedMap = keyof RouteNamedMap> =
    | RouteLocationAsString<RouteNamedMap>
    | RouteLocationAsRelativeTypedList<RouteNamedMap>[Name]
    | RouteLocationAsPathTypedList<RouteNamedMap>[Name]

  /**
   * Generate a type safe params for a route location. Requires the name of the route to be passed as a generic.
   */
  export type RouteParams<Name extends keyof RouteNamedMap> = RouteNamedMap[Name]['params']
  /**
   * Generate a type safe raw params for a route location. Requires the name of the route to be passed as a generic.
   */
  export type RouteParamsRaw<Name extends keyof RouteNamedMap> = RouteNamedMap[Name]['paramsRaw']

  export function useRouter(): RouterTyped
  export function useRoute<Name extends keyof RouteNamedMap = keyof RouteNamedMap>(name?: Name): RouteLocationNormalizedLoadedTypedList<RouteNamedMap>[Name]

  export const useLink: UseLinkFnTyped<RouteNamedMap>

  export function onBeforeRouteLeave(guard: NavigationGuard<RouteNamedMap>): void
  export function onBeforeRouteUpdate(guard: NavigationGuard<RouteNamedMap>): void

  export const RouterLink: RouterLinkTyped<RouteNamedMap>
  export const RouterLinkProps: RouterLinkPropsTyped<RouteNamedMap>

  // Experimental Data Fetching

  export function defineLoader<
    P extends Promise<any>,
    Name extends keyof RouteNamedMap = keyof RouteNamedMap,
    isLazy extends boolean = false,
  >(
    name: Name,
    loader: (route: RouteLocationNormalizedLoaded<Name>) => P,
    options?: _DefineLoaderOptions<isLazy>,
  ): _DataLoader<Awaited<P>, isLazy>
  export function defineLoader<
    P extends Promise<any>,
    isLazy extends boolean = false,
  >(
    loader: (route: RouteLocationNormalizedLoaded) => P,
    options?: _DefineLoaderOptions<isLazy>,
  ): _DataLoader<Awaited<P>, isLazy>

  export {
    _definePage as definePage,
    _HasDataLoaderMeta as HasDataLoaderMeta,
    _setupDataFetchingGuard as setupDataFetchingGuard,
    _stopDataFetchingScope as stopDataFetchingScope,
  } from 'unplugin-vue-router/runtime'
}

declare module 'vue-router' {
  import type { RouteNamedMap } from 'vue-router/auto/routes'

  export interface TypesConfig {
    beforeRouteUpdate: NavigationGuard<RouteNamedMap>
    beforeRouteLeave: NavigationGuard<RouteNamedMap>

    $route: RouteLocationNormalizedLoadedTypedList<RouteNamedMap>[keyof RouteNamedMap]
    $router: _RouterTyped<RouteNamedMap>

    RouterLink: RouterLinkTyped<RouteNamedMap>
  }
}
