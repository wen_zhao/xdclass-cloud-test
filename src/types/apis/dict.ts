export interface IDict {
  id: number
  category: string
  categoryName: string
  extend: string
  name: string
  value: string
  remark: string
}
