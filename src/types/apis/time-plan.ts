export interface ITimePlan {
  caseId: number
  executeTime: string
  gmtCreate: date
  gmtModified: date
  id: number
  isDisabled: number
  name: string
  projectId: number
  testType: string
}
